package test.itc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import test.itc.figuras.FiguraGeometrica;

public class TestDate {
	
	public static void main(String[] args) {
		TestDate main = new TestDate();
		//main.trabajandoConDate();
		//main.trabajandoConDate2();
		//main.trabajandoConLocalDate();
		//main.trabajandoConLocalTime();
		//main.trabajandoConLocalDateTime();
		//main.trabajandoConLocalDateTime2();
		main.trabajandoConOffsetDateTime();
		
		ArrayList<Integer> numeros;
		ArrayList<String> cadenas;
		ArrayList<FiguraGeometrica> figuras;
	}
	
	public void trabajandoConOffsetDateTime() {
		System.out.println(OffsetDateTime.now());
		System.out.println(ZonedDateTime.now());
		
		LocalDate date = LocalDate.of(2020, 1, 20);
		LocalTime time = LocalTime.of(20, 25);
		
		LocalDateTime dateTime = LocalDateTime.of(date, time);
		
		System.out.println(dateTime);
		
		System.out.println(dateTime.toLocalDate());
		System.out.println(dateTime.toLocalTime());
	}
	
	public void trabajandoConLocalDateTime2() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		
		System.out.println(dtf.format(LocalDateTime.now()));
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese la fecha en formato dd/MM/yyyy HH:mm: ");
		String dateAsString = scanner.nextLine();
		
		try {
			//TemporalAccessor date = dtf.parse(dateAsString);
			
			System.out.println(LocalDateTime.parse(dateAsString, dtf));
		}catch (DateTimeParseException e) {
			System.out.println("La fecha ingresada es invalida");
		}
		
		
		scanner.close();
	}
	
	public void trabajandoConLocalDate() {
		LocalDate date = LocalDate.now();
		System.out.println(date);
		
		System.out.println(date.getDayOfMonth());
		System.out.println(date.getMonth());
		System.out.println(date.getMonthValue());
		System.out.println(date.getYear());
		
		//Month.AUGUST;
		
		LocalDate date1 = LocalDate.of(2022, Month.JANUARY, 1);
		
		System.out.println(date1);
		
		System.out.println(date1.minusMonths(1));
		System.out.println(date1.minus(20, ChronoUnit.DAYS));
		
		System.out.println(date1.plusDays(25));
		System.out.println(date1.plus(2, ChronoUnit.MONTHS));
	}
	
	public void trabajandoConLocalTime() {
		LocalTime time = LocalTime.now();
		System.out.println(time);
		
		System.out.println(time.getHour());
		System.out.println(time.getMinute());
		System.out.println(time.getSecond());
		
		//Month.AUGUST;
		
		LocalTime time1 = LocalTime.of(10, 25);
		
		System.out.println(time1);
		
		System.out.println(time1.minusHours(10));
		System.out.println(time1.minus(20, ChronoUnit.MINUTES));
		
		System.out.println(time1.plusMinutes(25));
		System.out.println(time1.plus(2, ChronoUnit.HOURS));
	}
	
	public void trabajandoConLocalDateTime() {
		LocalDateTime date = LocalDateTime.now();
		System.out.println(date);
	}
	
	public void trabajandoConDate() {
		Date date = new Date();
		
		System.out.println(date);
		System.out.println(date.getTime());
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		System.out.println(cal.get(Calendar.DAY_OF_MONTH));
		System.out.println(cal.get(Calendar.DAY_OF_YEAR));
		System.out.println(cal.get(Calendar.WEEK_OF_MONTH));
		System.out.println(cal.get(Calendar.MONTH));
		System.out.println(cal.get(Calendar.HOUR_OF_DAY));
		System.out.println(cal.get(Calendar.MINUTE));
		
		Calendar cal1 = Calendar.getInstance();
		cal1.set(2022, Calendar.JANUARY, 1, 15, 20, 0);

		Date date1 = cal1.getTime();
		System.out.println(date1);
	}
	
	public void trabajandoConDate2() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
		sdf2.setLenient(false);
		
		Date date = new Date();
		
		System.out.println(date);
		System.out.println(sdf.format(date));
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese la fecha en formato dd-MM-yyyy: ");
		
		String dateAsString = scanner.nextLine();
		
		Date date2;
		try {
			date2 = sdf2.parse(dateAsString);
			
			System.out.println(date2);
		} catch (ParseException e) {
			System.out.println("Ingrese una fecha en formato dd-MM-yyyy");
			e.printStackTrace();
		}
		
		scanner.close();
	}

}
