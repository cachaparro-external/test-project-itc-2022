package test.itc.figuras;

/**Clase abstracta que agrupa las figuras geometricas
 * 
 * @author cachaparro
 * @since 1.0.0
 * @since 26/07/2022
 *
 */
public abstract class FiguraGeometrica {

	public void dibujar() {
		//Comentarios de una linea
		/*
		 * Comentarios
		 * de 
		 * multiples lineas
		 */
		
		System.out.println("Dibujando Figura Geometrica");
	}
	
}
