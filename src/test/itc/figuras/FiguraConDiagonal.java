package test.itc.figuras;

public interface FiguraConDiagonal {

	public final int x = 25;
	
	public double calcularDiagonal();
	
}
