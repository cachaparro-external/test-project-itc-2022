package test.itc.figuras.d2;

public class Isoceles extends Triangulo{

	private double[] lados;
	
	public Isoceles(double altura, double base, double[] lados) {
		super(altura, base);
		this.lados = lados;
	}

	@Override
	public double calcularPerimetro() {
		return this.lados[0] + this.lados[1] + this.lados[2];
	}
	
}
