package test.itc.figuras.d2;

public abstract class Triangulo extends Figura2D{
	
	private double altura;
	protected double base;
	
	public Triangulo(double altura, double base) {
		super(3);
		this.altura = altura;
		this.base = base;
	}

	public double calcularHipotenusa() {
		return 0;
	}

	@Override
	public double calcularArea() {
		return this.base * this.altura;
	}
}
