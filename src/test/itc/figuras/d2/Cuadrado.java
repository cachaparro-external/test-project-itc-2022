package test.itc.figuras.d2;

import test.itc.figuras.FiguraConDiagonal;

public class Cuadrado extends Figura2D implements FiguraConDiagonal{
	
	/**
	 * Lado del cuadrado
	 */
	private double lado;
	
	public Cuadrado(double lado) {
		super(4);
		this.lado = lado;
	}

	@Override
	public double calcularArea() {
		return lado * lado;
	}

	@Override
	public double calcularPerimetro() {
		return 4 * lado;
	}

	@Override
	public double calcularDiagonal() {
		return Math.sqrt(this.lado) / 2;
	}
}
