package test.itc.figuras.d2;

public class Elipse extends Circulo{

	@Override
	public double calcularPerimetro() {
		return 2 * Math.PI;
	}
	
}
