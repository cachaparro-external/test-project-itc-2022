package test.itc.figuras.d2;

public class Circulo extends Figura2D{
	
	private double radio;
	
	public Circulo() {
		super(0);
		this.radio = 0;
	}

	public Circulo(double radio) {
		super(0);
		this.radio = radio;
	}
	
	public double calcularDiametro() {
		return 2 * radio;
	}

	@Override
	public double calcularArea() {
		return Math.PI * radio * radio;
	}

	@Override
	public double calcularPerimetro() {
		return 2 * Math.PI * radio;
	}

	@Override
	public void dibujar() {
		System.out.println("Dibujando Circulo");
	}
	
	

}
