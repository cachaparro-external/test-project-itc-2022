package test.itc.figuras.d2;

public class Equilatero extends Triangulo{

	public Equilatero(double altura, double base) {
		super(altura, base);
	}

	@Override
	public double calcularPerimetro() {
		return 3 * super.base;
	}

}
