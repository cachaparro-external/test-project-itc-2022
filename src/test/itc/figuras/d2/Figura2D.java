package test.itc.figuras.d2;

import test.itc.figuras.FiguraGeometrica;

public abstract class Figura2D extends FiguraGeometrica{
	
	protected int lados;
	
	public Figura2D(int lados) {
		super();
		
		this.lados = lados;
	}
	
	public abstract double calcularPerimetro();
	
	public abstract double calcularArea();

	public int getLados() {
		return lados;
	}

	@Override
	public void dibujar() {
		System.out.println("Dibujando Figura 2D");
	}
	
	
}
