package test.itc.figuras.d3;

import test.itc.figuras.d2.Circulo;

public class Cono extends Figura3D {

	private double altura;
	
	public Cono(double altura, double radio) {
		super(new Circulo(radio));
		this.altura = altura;
	}
	
	/*public Cono(double altura, double radio) {
		super();
		this.altura = altura;
		super.base = new Circulo(radio);
	}*/
	
}
