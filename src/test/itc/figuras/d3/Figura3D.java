package test.itc.figuras.d3;

import test.itc.figuras.FiguraGeometrica;
import test.itc.figuras.d2.Figura2D;

public abstract class Figura3D extends FiguraGeometrica{

	protected Figura2D base;
	
	public Figura3D(Figura2D base) {
		super();
		this.base = base;	
	}
	
	public Figura3D() {
		super();
	}

}
