package test.itc.util;

public class TestVariables {
	
	private int num1 = 25;
	
	//Alcance: Clase
	public void test1() {
		num1 = 32;
		System.out.println("Valor: " + num1);
	}
	
	//Alcance: Método
	public void test2() {
		int num1 = 42;
		int num2 = 152;
		
		num2 = 451;
		System.out.println("Valor: " + num2);
		
		num1 = 74;
		System.out.println("Valor: " + num1);
		
		//this
		System.out.println("Valor de clase: " + this.num1);
	}
	
	public void test3() {
		for(int i=0; i<10; i++) {
			System.out.println(i);
		}
	}

}
