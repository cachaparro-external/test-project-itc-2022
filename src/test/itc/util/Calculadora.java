package test.itc.util;

import test.itc.inner.CalculoAngulo;
import test.itc.inner.CalculoRadianes;
import test.itc.inner.CalculoSexagecimales;

public class Calculadora {
	
	public static int valor;
	public static final String CADENA = "Cadena";
	
	private int var1;
	private int edad;
	public int var2;
	int var3;
	
	private CalculoAngulo calculoAngulo;
	
	public int sumar(int num1, int num2) {
		int suma = num1 + num2;
		this.imprimirResultado(suma);
		algo();
		System.out.println(CADENA);
		valor = 24;
		
		return suma;
	}
	
	public static int restar(int num1, int num2) {
		algo();
		valor = 1;
		System.out.println(CADENA);
		return num1 - num2;
	}
	
	public static void algo() {
		System.out.println("Algo");
	}
	
	private void imprimirResultado(int resultado) {
		System.out.println("El resultado es: " + resultado);
	}
	
	public void setVar1(int var1) {
		this.var1 = var1;
	}
	
	public int getVar1() {
		return var1;
	}

	public int getEdad() {
		return this.edad * 365;
	}

	public void setEdad(int edad) {
		if(edad >= 0 && edad <= 150) {
			this.edad = edad;
		}else {
			this.edad = 0;
		}
	}
	
	public void setAngleMode(int mode) {
		if(mode == 1) {//RAD
			this.calculoAngulo = new CalculoRadianes();
		}else if (mode == 2) {//SEXA
			this.calculoAngulo = new CalculoSexagecimales();
		}else if (mode == 3) {//CENTE
			this.calculoAngulo = new CalculoAngulo() {
				
				@Override
				public double calcularAngulo(int x1, int y1, int x2, int y2, int x3, int y3) {
					return 100;
				}
			};
		}else {
			throw new IllegalArgumentException("El modeo no esta soportado");
		}
	}
	
	public double obtenerAngulo(int x1, int y1, int x2, int y2, int x3, int y3) {
		assert this.calculoAngulo != null : "Tiene que llamar primero al metodo setAngleMode";
		
		return this.calculoAngulo.calcularAngulo(x1, y1, x2, y2, x3, y3);
	}
	
	

	@Override
	public String toString() {
		return "Los valores de mi clase Calculadora son " + var1 + " - " + edad;
	}
	
	
}
