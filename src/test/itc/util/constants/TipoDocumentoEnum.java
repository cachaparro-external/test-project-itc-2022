package test.itc.util.constants;

public enum TipoDocumentoEnum {
	
	CC(1, "Cédula"),
	TI(2, "Tarjeta ID"),
	RC(3, "Registro Civil");
	
	private int id;
	private String nombreLargo;
	
	private TipoDocumentoEnum(int id, String nombreLargo) {
		this.id = id;
		this.nombreLargo = nombreLargo;
	}

	public int getId() {
		return id;
	}

	public String getNombreLargo() {
		return nombreLargo;
	}	
	
	public static TipoDocumentoEnum findById(int id) {
		for(TipoDocumentoEnum tipoDoc : values()) {
			if(id == tipoDoc.getId()) {
				return tipoDoc;
			}
		}
		
		throw new IllegalArgumentException("TipoDocumentoEnum con ID  " + id + " no existe");
	}
}
