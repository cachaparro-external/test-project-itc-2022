package test.itc.util.constants;

public interface MathConstants {
	
	public static final double PHI = 3.1425;
	public static final double E = 2.8184;

}
