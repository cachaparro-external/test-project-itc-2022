package test.itc.util.exception;

public class EmpleadoNoExisteException extends Exception {

	public EmpleadoNoExisteException() {
		super();
	}

	public EmpleadoNoExisteException(String message, Throwable cause) {
		super(message, cause);
	}

	public EmpleadoNoExisteException(String message) {
		super(message);
	}

	public EmpleadoNoExisteException(Throwable cause) {
		super(cause);
	}

}
