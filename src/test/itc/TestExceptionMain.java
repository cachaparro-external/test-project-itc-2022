package test.itc;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.math.NumberUtils;

import test.itc.util.exception.EmpleadoNoExisteException;

public class TestExceptionMain {

	public static void main(String[] args) {
		TestExceptionMain main = new TestExceptionMain();
		
		/*try {
			main.nivel1();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println("Ejecutando bloque finally");
		}*/
		
		try {
			main.trabajandoConExcepciones1();
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Esta tratando de acceder a un valor no valido: " + e.getMessage());
		}catch (NumberFormatException | ArithmeticException e) {
			e.printStackTrace();
			System.out.println("Esta tratando de convertir un numero no valido: " + e.getMessage());
		/*}catch (ArithmeticException e) {
			e.printStackTrace();
			System.out.println("Tratando de dividir por cero: " + e.getMessage());*/
		}catch (RuntimeException e) {
			System.out.println("Error en tiempo de ejecución: " + e.getMessage());
		}catch (Exception e) {
			System.out.println("Error");
		}finally {
			System.out.println("Terminando programa");
		}
		
		try {
			main.trabajandoConExcepciones2();
		} catch (EmpleadoNoExisteException e) {
			System.out.println("El empleado no existe");
			
			e.printStackTrace();
		}
		
		//main.trabajandoConExcepciones3();
		try {
			main.trabajandoConExcepciones4();
		}catch (AssertionError e) {
			System.err.println("Los datos ingresados son invalidos: " + e.getMessage());
		}
		
	}
	
	public void trabajandoConExcepciones4() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese un numero:");
		String numero = scanner.nextLine();
		
		assert NumberUtils.isParsable(numero);
		
		Integer numeroInt = Integer.valueOf(numero);
			
		System.out.println("Mi numero es: " + numeroInt);
			
		assert numeroInt != 0 : "División entre cero no es válida";
		
		System.out.println("La división es " + (100/numeroInt));
		
		scanner.close();
		
		//StringUtils -> IllegarArgumentException
		//NumberUtils
		//DateUtils
	}
	
	public void trabajandoConExcepciones3() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese un numero:");
		String numero = scanner.nextLine();
		
		try {
			Integer numeroInt = Integer.valueOf(numero);
			
			System.out.println("Mi numero es: " + numeroInt);
			
			if(numeroInt != 0) {
				System.out.println("La división es " + (100/numeroInt));
			}else {
				System.err.println("No puede hacer la división por cero");
			}
		}catch (NumberFormatException e) {
			System.err.println("El valor ingresado no es un numero");
		}finally {
			scanner.close();
		}
	}
	
	/**
	 * 
	 * @throws EmpleadoNoExisteException No existe el empleado consultado
	 */
	public void trabajandoConExcepciones2() throws EmpleadoNoExisteException, ArithmeticException {
		Map<String, Integer> empleados = new HashMap<>();
		
		empleados.put("Cesar", 200);
		empleados.put("Lorena", 500);
		empleados.put("Jose", 1000);
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese el nombre de la persona:");
		String name = scanner.nextLine();
		
		scanner.close();
		
		if(empleados.containsKey(name)) {
			System.out.println("El empleado existe");
		}else {
			/*EmpleadoNoExisteException e = new EmpleadoNoExisteException("El empleado " + name + " no existe");
			
			throw e;*/
			
			throw new EmpleadoNoExisteException("El empleado " + name + " no existe");
		}
	}
	
	public void trabajandoConExcepciones1() {
		int[] numeros = {1, 2, 3, 4};
		
		for(int i=0; i<numeros.length; i++) {
			System.out.println("Arreglo[" + i + "]= " + numeros[i]);
		}
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese un numero:");
		String numero = scanner.nextLine();
		
		Integer numeroInt = Integer.valueOf(numero);
		
		System.out.println("Mi numero es: " + numeroInt);
		
		System.out.println("La división es " + (100/numeroInt));
		
		scanner.close();
		
		//throw new RuntimeException("Mi excepción");
	}
	
	public void nivel1() throws Exception {
		System.out.println("Nivel 1");
		this.nivel2();
	}
	
	public void nivel2() throws Exception {
		System.out.println("Nivel 2");
		
		this.nivel3();
	}
	
	/*public void nivel3() {
		try {
			System.out.println("Nivel 3");
			
			throw new Exception("My Exception");
		}catch (Exception e) {
			e.printStackTrace();
			
			System.out.println("Atrapando y procesando excepción: " + e.getMessage());
		}
	}*/
	
	public void nivel3() throws Exception {
		System.out.println("Nivel 3");
			
		throw new Exception("My Exception");
	}


}
