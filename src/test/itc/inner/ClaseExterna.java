package test.itc.inner;

public class ClaseExterna {
	
	private int valor;
	
	public void imprimirValor() {
		System.out.println(valor);
	}
	
	public class ClaseInterna {
		private int valorInt;
		
		public void imprimirValorInt() {
			System.out.println(valorInt);
			
			valor = 45;
			imprimirValor();
		}
	}
}
