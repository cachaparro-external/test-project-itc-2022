package test.itc.inner;

public class ClaseExterna3 {
	
	private int valor;
	
	public void imprimir() {
		System.out.println(valor);
	}
	
	public void creandoClaseLocal() {
		
		class ClaseLocal{
			private int valorLocal;
			
			public void imprimirLocal() {
				System.out.println(valorLocal);
			}
		}
		
		ClaseLocal local = new ClaseLocal();
		local.imprimirLocal();
	}
	

}
