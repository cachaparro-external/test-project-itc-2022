package test.itc.inner;

public class ClaseExterna2 {
	
	private int valor;
	private static int valorStatic;
	
	public void imprimirValor() {
		System.out.println(valor);
	}
	
	public static class ClaseInterna2 {
		private int valorInt;
		
		public void imprimirValorInt() {
			System.out.println(valorInt);
			
			valorStatic = 23;
		}
	}
}
