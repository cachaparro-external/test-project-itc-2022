package test.itc.inner;

public interface CalculoAngulo {

	public double calcularAngulo(int x1, int y1, int x2, int y2, int x3, int y3);
	
}
