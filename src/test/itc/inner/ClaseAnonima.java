package test.itc.inner;

public class ClaseAnonima {

	private int valor;
	
	public ClaseAnonima(int valor) {
		this.valor = valor;
	}
	
	public void imprimir() {
		System.out.println(valor);
	}
	
}
