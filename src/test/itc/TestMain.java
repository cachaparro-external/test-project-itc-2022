package test.itc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

import test.itc.inner.ClaseAnonima;
import test.itc.inner.ClaseExterna;
import test.itc.inner.ClaseExterna.ClaseInterna;
import test.itc.inner.ClaseExterna2;
import test.itc.inner.ClaseExterna2.ClaseInterna2;
import test.itc.util.Calculadora;
import test.itc.util.constants.EstadosRegistroEnum;
import test.itc.util.constants.TipoDocumentoEnum;

public class TestMain { //UCC
	
	public static void main(String[] args) {
		System.out.println("Hola Mundo");
		
		//COmentario de una sola linea
		
		/*
		 * Comentarios 
		 * de 
		 * multiples
		 * lineas
		 */
		
		/*int numero; //LCC
		numero = 25;
		
		System.out.println("El numero es " + numero);
		
		double interesCorriente = 85.12;
		
		System.out.println("El numero es " + interesCorriente);
		
		char caracter = 111;
		char caracter2 = 'A';
		
		System.out.println("El caracter 1 es " + caracter);
		System.out.println("El caracter 2 es " + caracter2);
		
		boolean decision = false;
		
		System.out.println("La decisión es " + decision);
		
		long numeroLargo = 35L;
		
		System.out.println("El numero Long es: " + numeroLargo);
		
		float numeroFlotante = 25.36F;
		
		System.out.println("El numero Float es: " + numeroFlotante);
		
		numeroFlotante = 78.2F;
		
		System.out.println("El numero Float es: " + numeroFlotante);
		
		final double NUMERO_PHI = 3.1416;
		
		System.out.println("El numero PHI es: " + NUMERO_PHI);
		
		String cadena1;
		cadena1 = "Mi cadena 1";
		
		System.out.println(cadena1);
		
		String cadena2 = "Mi cadena 2";
		
		System.out.println("La cadena es: " + cadena2);*/
		
		//trabajandoConString();
		//trabajandoConOperadoresAritmeticos();
		//trabajandoConOperadoresAritmeticos2();
		//trabajandoConOperadoresRelacionales();
		//trabajandoConOperadoresLogicos();
		//trabajandoConIf();
		//trabajandoConSwitch();
		//trabajandoConIncrementos();
		//trabajandoConWhile();
		//trabajandoConDoWhile();
		//trabajandoConFor();
		//trabajandoConFor2();
		//trabajandoConFor3();
		//trabajandoConArreglos();
		//trabajandoConArreglos2();
		
		int x = 5;
		
		TestMain main = new TestMain();
		//main.trabajandoConMetodosNoEstaticos();
		
		//int respuesta = main.trabajandoConMetodos();
		//main.trabajandoConMetodos();
		
		//System.out.println("Respuesta de trabajandoConMetodos: " + respuesta);
		
		//System.out.println("Respuesta de sumar: " + main.sumar(5, 2));
		
		/*int num1, num2;
		num1 = 5;
		num2 = 2;
		
		AtomicInteger suma, resta;
		suma = new AtomicInteger(0);
		resta = new AtomicInteger(0);
		
		main.sumarRestar(num1, num2, suma, resta);
		
		System.out.println("La suma es: " + suma.get());
		System.out.println("La resta es: " + resta.get());*/
		
		Calculadora calculadora = new Calculadora();
		//int suma = calculadora.sumar(5, 2);
		
		//System.out.println("La suma es: " + suma);
		
		calculadora.sumar(1, 2);
		
		Calculadora.restar(5, 2);
		System.out.println(Calculadora.CADENA);
		
		//new String().
		
		
		//calculadora.setVar1(12);
		//int valorVar1 = calculadora.getVar1();
		
		//System.out.println("Valor de var 1: " + valorVar1);
		//System.out.println("Valor de var 1: " + calculadora.getVar1());
		
		/*calculadora.setEdad(25);
		
		System.out.println("La edd es: " + calculadora.getEdad());
		
		calculadora.setEdad(-5);
		
		System.out.println("La edd es: " + calculadora.getEdad());
		
		calculadora.var2 = 40;
		System.out.println("El valor de var2 es: " + calculadora.var2);
		
		calculadora.var3*/
		
		/*TestVariables testVar = new TestVariables();
		testVar.test2();*/
		//main.ejemplo();
		
		//EstadosRegistro.ACTIVO
		//EstadosRegistro.INACTIVO;
		
		//MathConstants.E
		//MathConstants.PHI
		
		//EstadosRegistroEnum.ACTIVO
		//EstadosRegistroEnum.INACTIVO
		
		/*AtomicInteger suma = new AtomicInteger();
		AtomicInteger resta = new AtomicInteger();
		
		main.sumarRestar(5, 2, suma, resta);
		
		System.out.println("Suma: " + suma);
		System.out.println("Resta: " + resta);*/
		
		//main.trabajandoConEnumeraciones();
		//main.trabajandoConEnumeraciones2();
		//main.trabajandoConObject();
		main.trabajandoConClasesAnonimas();
	}
	
	public void trabajandoConClasesAnonimas() {
		new ClaseAnonima(25).imprimir();
		
		Calculadora calculadora = new Calculadora();
		calculadora.setAngleMode(3);
		
		double angle = calculadora.obtenerAngulo(1,  1, 0, 0, 0, 0);
		
		System.out.println("Angulo: " + angle);
	}
	
	public void trabajandoConClasesInternas() {
		ClaseExterna externa = new ClaseExterna();
		//Inner No Static
		ClaseInterna interna = externa.new ClaseInterna();
		ClaseExterna.ClaseInterna interna1 = externa.new ClaseInterna();
		//Inner Static
		ClaseInterna2 interna2 = new ClaseInterna2();
		ClaseExterna2.ClaseInterna2 interna21 = new ClaseExterna2.ClaseInterna2();
	}
	
	
	public void trabajandoConObject() {
		int[] numeros = {1, 2, 3};
		
		System.out.println("Numeros: " + numeros);
		
		List<Integer> collection = new ArrayList<>();
		
		collection.add(1);
		collection.add(2);
		collection.add(3);
		
		System.out.println("List: " + collection);
		
		Calculadora calculadora = new Calculadora();
		
		System.out.println("Calculadora: " + calculadora.toString());
		
		System.out.println("Paquete: " + calculadora.getClass().getPackageName());
		System.out.println("Simple Name: " + calculadora.getClass().getSimpleName());
		System.out.println("CanonicalName: " + calculadora.getClass().getCanonicalName());
		System.out.println("Name: " + calculadora.getClass().getName());
		System.out.println("Clase: " + calculadora.getClass());
		calculadora.setVar1(564);
		
		System.out.println("Hashcode " + calculadora.hashCode());
		
		Calculadora calculadora1 = new Calculadora();
		
		System.out.println("Iguales: " + calculadora.equals(calculadora1));
		System.out.println("Iguales: " + calculadora.equals(calculadora));
		
	}
	
	public void trabajandoConEnumeraciones2() {
		System.out.println(TipoDocumentoEnum.CC.getId() + " " + TipoDocumentoEnum.CC.getNombreLargo());
		
		System.out.println(TipoDocumentoEnum.findById(1).getNombreLargo());
		System.out.println(TipoDocumentoEnum.findById(15).getNombreLargo());	
	}
	
	
	public void trabajandoConEnumeraciones() {
		/*for(EstadosRegistroEnum estadoRegistro : EstadosRegistroEnum.values()) {
			System.out.println("Estado registro: " + estadoRegistro);
		}
		
		System.out.println("Enum1: " + EstadosRegistroEnum.valueOf("ACTIVO"));
		System.out.println("Enum2: " + EstadosRegistroEnum.valueOf("HABILITADO"));
		
		System.out.println("Ingrese el estado");*/
		
		Scanner scanner = new Scanner(System.in);
		
		String estado = scanner.nextLine();
		
		try {
			EstadosRegistroEnum estadoEncontrado = EstadosRegistroEnum.valueOf(estado);
			
			if(estadoEncontrado.equals(EstadosRegistroEnum.ACTIVO)) {
				System.out.println("El registro esta activo");
			}else {
				System.out.println("El registro esta no activo");
			}
		}catch (IllegalArgumentException e) {
			System.out.println("El estado ingresado no existe");
		}
		
		scanner.close();
	}
	
	
	public void ejemplo() {
		String name = "Daniel";
		
		/*ArrayList<String> nameSplit = new ArrayList<>();
		
		for(int i = 0 ; i < name.length(); i++) {
			nameSplit.add(name.substring(i, i+1));
		}*/
		
		String[] nameSplitArray = name.split("");
		
		for(String n : nameSplitArray) {
			System.out.println(n);
		}
		
		List<String> nameSplit = Arrays.asList(nameSplitArray);
		
		System.out.println(nameSplitArray);
		System.out.println(nameSplit);
		
		Collections.reverse(nameSplit);
		
		System.out.println(nameSplit);
	}
	
	public void trabajandoConMetodosNoEstaticos() {
		System.out.println("Mensaje desde metodo trabajandoConMetodosNoEstaticos");
	}
	
	public int trabajandoConMetodos() {
		System.out.println("Mensaje desde metodo trabajandoConMetodos");
		
		return 25;
	}
	
	/**Método que suma dos números
	 * 
	 * @param num1 Primer número
	 * @param num2 Segundo Número
	 * @return Suma de los numeros
	 * 
	 */
	public int sumar(int num1, int num2) {
		System.out.println("Mensaje desde metodo sumar");
		
		int suma = num1 + num2;
		
		//return num1 + num2;
		
		return suma;
	}
	
	public void sumarRestar(final int num1, final int num2, int suma, int resta) {
		System.out.println("Mensaje desde metodo sumarRestar");
		
		suma = num1 + num2;
		resta = num1 - num2;
		
		return;
	}
	
	public void sumarRestar(final int num1, final int num2, AtomicInteger suma, AtomicInteger resta) {
		System.out.println("Mensaje desde metodo sumarRestar");
		
		suma.set(num1 + num2);
		resta.set(num1 - num2);
		
		return;
	}
	
	public static void trabajandoConArreglos2() {
		Scanner scanner = new Scanner(System.in);
		
		int[] arreglo1; 
		
		System.out.println("Ingrese la cantidad de numeros a guardar: ");
		int cantidadNumeros = scanner.nextInt();
		
		arreglo1 = new int[cantidadNumeros];
		
		for(int i = 0; i < arreglo1.length; i++) {
			System.out.println("Ingrese el elemento de la posición " + i + ": ");
			arreglo1[i] = scanner.nextInt();
		}
		
		System.out.println("El arreglo ingresado es: ");
		
		for(int numero : arreglo1) {
			System.out.print(numero + " ");
		}
		
		System.out.println("");
		
		scanner.close();
	}
	
	public static void trabajandoConArreglos() {
		Scanner scanner = new Scanner(System.in);
		
		int[] arreglo1; 
		arreglo1 = new int[5];
		
		arreglo1[0] = 5;
		arreglo1[1] = 10;
		arreglo1[2] = 5;
		arreglo1[3] = 17;
		arreglo1[4] = 1;
		//arreglo1[5] = 18;
		
		int index = 0;
		
		while(index < arreglo1.length) {
			System.out.println("El elemento " + index + " es " + arreglo1[index]);
			index++;
		}
		
		int[] arreglo2 = new int[4];
		
		for(int i = 0; i < arreglo2.length; i++) {
			System.out.println("Ingrese el elemento de la posición " + i + ": ");
			int numero = scanner.nextInt();
			arreglo2[i] = numero;
		}
		
		System.out.println("El arreglo ingresado es: ");
		
		for(int i = 0; i < arreglo2.length; i++) {
			System.out.print(arreglo2[i] + " ");
		}
		
		System.out.println("");
		
		int[] arreglo3 = {1, 3, 5, 7, 9};
		
		System.out.println("El arreglo ingresado es: ");
		
		for(int numero : arreglo3) {
			System.out.print(numero + " ");
		}
		
		System.out.println("");
		
		scanner.close();
	}
	
	public static void trabajandoConFor3() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese hasta el numero que quiere imprimir:");
		int tope = scanner.nextInt();
		
		for(int contador = 1; contador <= tope; contador++) {
			if(contador == 20) {
				break;
			}
			
			System.out.print(contador + " ");
		}
		
		/*for(int contador = 100; contador <= tope && tope <= 200; contador*=5) {
			if(contador == 20) {
				break;
			}
			
			System.out.print(contador + " ");
		}*/
		
		scanner.close();
	}
	
	public static void trabajandoConFor2() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese hasta el numero que quiere imprimir (pares):");
		int tope = scanner.nextInt();
		
		for(int contador = 1; contador <= tope; contador++) {
			if(contador % 2 != 0) {
				continue;
			}
			
			System.out.print(contador + " ");
		}
		
		scanner.close();
	}
	
	public static void trabajandoConFor() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese hasta el numero que quiere imprimir:");
		int tope = scanner.nextInt();
		
		for(int contador = 1; contador <= tope; contador++) {
			System.out.print(contador + " ");
		}
		
		scanner.close();
	}
	
	public static void trabajandoConDoWhile() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese hasta el numero que quiere imprimir:");
		int tope = scanner.nextInt();
		
		int contador = 1;
		
		do {
			System.out.print(contador + " ");
			contador++;
		}while(contador <= tope);
		
		scanner.close();
	}
	
	public static void trabajandoConWhile() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese hasta el numero que quiere imprimir:");
		int tope = scanner.nextInt();
		
		int contador = 1;
		
		while(contador <= tope) {
			System.out.print(contador + " ");
			contador++;
		}
		
		scanner.close();
	}
	
	
	
	public static void trabajandoConIncrementos() {
		int contador = 0;
		
		System.out.println("Contador: " + contador);
		
		contador = contador + 1;
		
		System.out.println("Contador: " + contador);
		
		//contador+=2;
		contador+=1;
		
		System.out.println("Contador: " + contador);
		
		contador++;
		
		System.out.println("Contador: " + contador);
		
		contador = 10;
		
		System.out.println("Contador: " + contador);
		
		contador = contador - 1;
		
		System.out.println("Contador: " + contador);
		
		contador-=1;
		
		System.out.println("Contador: " + contador);
		
		contador--;
		
		System.out.println("Contador: " + contador);
		
		contador*=3;
		contador/=2;
		contador%=5;
		
		contador++;
		++contador;
		
		contador--;
		--contador;
	}
	
	public static void trabajandoConSwitch() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese un caracter: ");
		String cadena = scanner.nextLine();
		
		char primerCaracter = cadena.charAt(0);
		//cadena.startsWith("A");
		
		/*if(primerCaracter == 'a') {
			System.out.println("Empieza por vocal");
		}else if(primerCaracter == 'e') {
			System.out.println("Empieza por vocal");
		}else if(primerCaracter == 'i') {
			System.out.println("Empieza por vocal");
		}else if(primerCaracter == 'o') {
			System.out.println("Empieza por vocal");
		}else if(primerCaracter == 'u') {
			System.out.println("Empieza por vocal");
		}else {
			System.out.println("Empieza por consonante");
		}*/
		
		/*if(primerCaracter == 'a' || primerCaracter == 'e' || primerCaracter == 'i' || primerCaracter == 'o' || primerCaracter == 'u') {
			System.out.println("Empieza por vocal");
		}else {
			System.out.println("Empieza por consonante");
		}*/
		
		/*switch(primerCaracter) {
			case 'a':
				System.out.println("Empieza por vocal");
				break;
			case 'e':
				System.out.println("Empieza por vocal");
				break;
			case 'i':
				System.out.println("Empieza por vocal");
				break;
			case 'o':
				System.out.println("Empieza por vocal");
				break;
			case 'u':
				System.out.println("Empieza por vocal");
				break;
			default:
				System.out.println("Empieza por consonante");
		}*/
		
		switch(primerCaracter) { //numeros, caracteres, String
			case 'a': case 'e': case 'i': case 'o': case 'u':
				System.out.println("Empieza por vocal");
				break;
			default:
				System.out.println("Empieza por consonante");
		}
		
		/*switch("Algo") { //numeros, caracteres, String
		case "Casa": case "Perro": 
			System.out.println("Empieza por vocal");
			break;
		default:
			System.out.println("Empieza por consonante");
		}*/
		
		scanner.close();
	}
	
	
	public static void trabajandoConIf() {
		
		/*if(true) {
			
		}
		
		if(true) {
			//true
		}else {
			//false
		}
		
		if(true) {
			
		}else if(false) {
			
		}else if(false) {
			
		}else {
			
		}
		
		if(true) {
			
		}else {
			if(false) {
				
			}else {
				
			}
		}*/
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese el primer numero: ");
		int num1 = scanner.nextInt();
		
		System.out.println("Ingrese el segundo numero: ");
		int num2 = scanner.nextInt();
		
		if(num1 > num2) {
			System.out.println("El primer numero es mayor que el segundo numero");
		}else {
			System.out.println("El segundo numero es mayor que el primer numero");
		}
		
		scanner.close();
		
	}
	
	public static void trabajandoConCast() {
		short s = 23;
		int i = 451233;
		long l = 21212121212123L;
		
		float f = 3.25F;
		double d = 45.1212122121;
		
		int i2 = s;
		long l2 = i;
		
		short s1 = (short)i;
		int i3 = (int)l;
		
		double d2 = f;
		float f2 = (float)d;
		
		char c = 45;
		char c1 = 'C';
		
		String st = String.valueOf(l);
		
		int i4 = Integer.valueOf("25");
		
		//long -> Long
		//short -> Short
		//float -> Float
		//double -> Double
		//char -> Character
		//boolean -> Boolean
		
		
		
	}
	
	public static void trabajandoConOperadoresLogicos() {
		// AND -> && & -> V && V = V
		// OR -> || | -> F || F = F
		// NOT -> !
		
		boolean valor1 = false, valor2= false;
		
		System.out.println("AND: " +  (valor1 && valor2));
		System.out.println("OR: " +  (valor1 || valor2));
		System.out.println("NOT: " +  (!valor1));
	}
	
	public static void trabajandoConString() { //LCC
		String cadena1 = "Buenos días";
		String cadena2 = "Buenas noches";
		
		int longitudCadena1 = cadena1.length();
		
		System.out.println("La longitud de la cadena " + cadena1 + " es: " + longitudCadena1);
		System.out.println("La longitud de la cadena " + cadena2 + " es: " + cadena2.length());
		
		String cadenaConcatenada = cadena1.concat(cadena2);
		
		System.out.println("La cadena concatenada es: " + cadenaConcatenada);
		System.out.println("La cadena concatenada es: " + cadena1.concat(" ").concat(cadena2));
		
		System.out.println("La cadena original es: " + cadena1 + " la cadena modificada es " + cadena1.replace('o', '0').replace('s', '5'));
	}
	
	public static void trabajandoConOperadoresRelacionales() {
		// > >= < <= == !=
		int num1 = 3, num2 = 5;
		boolean mayor, mayorIgual, menor, menorIgual, igual, diferente;
		
		mayor = num1 > num2;
		mayorIgual = num1 >= num2;
		menor = num1 < num2;
		menorIgual = num1 <= num2;
		igual = num1 == num2;
		diferente = num1 != num2;
		
		System.out.println("Num1 > Num2: " + mayor);
		System.out.println("Num1 >= Num2: " + mayorIgual);
		System.out.println("Num1 < Num2: " + menor);
		System.out.println("Num1 <= Num2: " + menorIgual);
		System.out.println("Num1 == Num2: " + igual);
		System.out.println("Num1 != Num2: " + diferente);
	}
	
	public static void trabajandoConOperadoresAritmeticos() {
		//+ - * / %
		int num1, num2;
		int suma, resta, producto, division, residuo;
		
		//int num1 = 5, num2 = 4;
		
		num1 = 5;
		num2 = 4;
		
		suma = num1 + num2;
		resta = num1 - num2;
		producto = num1 * num2;
		division = num1 / num2;
		residuo = num1 % num2;
		
		System.out.println("La suma es: " + suma);
		System.out.println("La resta es: " + resta);
		System.out.println("La multiplicación es: " + producto);
		System.out.println("La división es: " + division);
		System.out.println("El residuo es: " + residuo);
		
		int resultado = 2 + 7 * 5 - 3 / 2 + 1;
		//2 + 35 - 3 / 2 + 1
		//2 + 35 - 1 + 1
		//37 - 1 + 1
		//36 + 1
		//37
		
		System.out.println("El resultado es: " + resultado);
		
		int resultado2 = ((((2 + 7) * 5) - 3) / 2) + 1;
		System.out.println("El resultado 2 es: " + resultado2);
	}
	
	public static void trabajandoConOperadoresAritmeticos2() {
		//+ - * / %
		double num1, num2;
		double suma, resta, producto, division, residuo;
		
		//int num1 = 5, num2 = 4;
		
		num1 = 5.4;
		num2 = 4.1;
		
		suma = num1 + num2;
		resta = num1 - num2;
		producto = num1 * num2;
		division = num1 / num2;
		//residuo = num1 % num2;
		
		System.out.println("La suma es: " + suma);
		System.out.println("La resta es: " + resta);
		System.out.println("La multiplicación es: " + producto);
		System.out.println("La división es: " + division);
		//System.out.println("El residuo es: " + residuo);
	}

}
