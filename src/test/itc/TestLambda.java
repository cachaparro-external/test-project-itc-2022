package test.itc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class TestLambda {

	
	public static void main(String[] args) {
		TestLambda lambda = new TestLambda();
		
		lambda.trabajandoConLambda();
	}
	
	public void trabajandoConLambda() {
		List<Integer> numeros = new ArrayList<>();
		
		Random random = new Random(System.currentTimeMillis());
		
		numeros.add(random.nextInt(10));
		numeros.add(random.nextInt(10));
		numeros.add(random.nextInt(10));
		numeros.add(random.nextInt(10));
		numeros.add(random.nextInt(10));
		numeros.add(random.nextInt(10));
		
		/*numeros.forEach((item) -> {
			System.out.println("Numero: " + item);
		});	
		
		System.out.println("Otra impresión");
		
		numeros.forEach(item -> System.out.println("Numero: " + item));
		
		System.out.println("Otra impresión");*/
		
		numeros.stream().forEach((item) -> {
			System.out.println("Numero: " + item);
		});
		
		/*numeros.stream().filter((item) -> {
			return item > 50;
		}).forEach(item -> System.out.println("Resultados: " + item));*/
		
		numeros.stream().sorted().forEach(item -> System.out.println("Resultado: " + item));
		
		/*int suma = numeros.stream().mapToInt(item -> item.intValue()).sum();
		
		System.out.println("La suma de los números es: " + suma);
		
		System.err.println("Arreglo original: " + numeros);*/
		
		boolean todosMenores = numeros.stream().allMatch(item -> item < 101);
		boolean algunoMenor50 = numeros.stream().anyMatch(item -> item < 50);
		
		//System.out.println(algunoMenor50);
		
		/*numeros.add(25);
		numeros.add(40);
		numeros.add(25);
		
		numeros.stream().distinct().forEach(item -> System.out.println(item));*/
		
		Predicate<Integer> mayorQue20 = (numero) -> {return numero >= 20;};
		//Predicate<Integer> mayorQue20 = numero -> numero >= 20;
		Predicate<Integer> menorQue60 = numero -> numero <= 60;
		
		numeros.stream().filter(mayorQue20).filter(menorQue60).forEach(item -> System.out.println(item)); //[20, 60]
		numeros.stream().filter(menorQue60.and(mayorQue20)).forEach(item -> System.out.println(item)); //[20, 60]
		numeros.stream().filter(menorQue60.and(mayorQue20).negate()).forEach(item -> System.out.println(item)); //[0, 20) U (60, 100]
		
		numeros.stream().mapToInt(item -> item.intValue()).max();
		numeros.stream().mapToInt(item -> item.intValue()).min();
		
		
		BinaryOperator<Integer> productoria = (Integer num1, Integer num2) -> {return num1 * num2;};
		//BinaryOperator<Integer> productoria = (num1, num2) -> num1 * num2;
		
		UnaryOperator<Integer> sumar100 = num1 -> num1 + 100;
		
		
		Optional<Integer> resultado = numeros.stream().reduce(productoria);
		
		if(resultado.isPresent()) {
			System.out.println("La productoria es: " + resultado.get());
		}
		
		numeros.stream().map(sumar100).forEach(item -> System.out.println(item));
				
		System.err.println("Arreglo original: " + numeros);
	}
	
}
