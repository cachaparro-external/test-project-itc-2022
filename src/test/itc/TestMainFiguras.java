package test.itc;

import test.itc.figuras.FiguraConDiagonal;
import test.itc.figuras.d2.Circulo;
import test.itc.figuras.d2.Cuadrado;
import test.itc.figuras.d2.Isoceles;

public class TestMainFiguras {

	public static void main(String[] args) {
		TestMainFiguras main = new TestMainFiguras();
		//main.trabajandoConFiguras1();
		main.trabajandoConInterfaces();
	}
	
	public void trabajandoConFiguras1() {

		Circulo circulo = new Circulo(5.2);
		
		System.out.println("Diametro: " + circulo.calcularDiametro());
		System.out.println("Area: " + circulo.calcularArea());
		System.out.println("Perimetro: " + circulo.calcularPerimetro());
		System.out.println("Lados: " + circulo.getLados());
		circulo.dibujar();
		
		double[] lados = {1.2, 3.4, 7.8};
		Isoceles isoceles = new Isoceles(2.4, 3.6, lados);
		
		System.out.println("Hipotenusa: " + isoceles.calcularHipotenusa());
		System.out.println("Area: " + isoceles.calcularArea());
		System.out.println("Perimetro: " + isoceles.calcularPerimetro());
		System.out.println("Lados: " + isoceles.getLados());
		isoceles.dibujar();
	}
	
	public void trabajandoConInterfaces() {
		FiguraConDiagonal figura = new Cuadrado(3.5);
		double diagonal = figura.calcularDiagonal();
		System.out.println("Diagonal: " + diagonal);
	}
	

}
