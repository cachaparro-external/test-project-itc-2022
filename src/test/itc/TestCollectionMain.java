package test.itc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TestCollectionMain {

	public static void main(String[] args) {
		TestCollectionMain main = new TestCollectionMain();
		//main.trabajandoConArrayList();
		//main.trabajandoConLinkedList();
		//main.trabajandoConSet();
		
		//Queue<E>
		//Stack<E>
		
		main.trabajandoConMap();
	}
	
	public void trabajandoConMap(){
		HashMap<Integer, String> map = new HashMap<>();
		
		map.put(30, "Jose");
		map.put(23, "Juan");
		map.put(50, "Ramiro");
		
		System.out.println("Map: " + map);
		
		String name = map.get(30);
		System.out.println("El nombre con key 30 es: " + name);
		System.out.println("El nombre con key 999 es: " + map.get(999));
		System.out.println("El nombre con key 999 es: " + map.getOrDefault(999, "No existe"));
		
		System.out.println("existe el key 50: " + map.containsKey(50)); 
		System.out.println("existe el valor 'Carlos': " + map.containsValue("Carlos")); 
		
		map.put(23, "Pedro");
		
		System.out.println("Map: " + map);
		
		System.out.println("Remove : " + map.remove(50));
		System.out.println("Remove : " + map.remove(510));
		
		System.out.println("Map: " + map);
		
		/*System.out.print("Keys: ");
		for(Integer key : map.keySet()) {
			System.out.print(key + ",");
		}*/
		
		/*System.out.print("Values: ");
		for(String value : map.values()) {
			System.out.print(value + ",");
		}*/
		
		System.out.print("Map: ");
		for(Entry<Integer, String> parejaClaveValor : map.entrySet()) {
			System.out.println("Key: " + parejaClaveValor.getKey() + " - Value: " + parejaClaveValor.getValue());
		}
		
		Map<Integer, String> map1 = new HashMap<>();
	}
	
	public void trabajandoConSet() {
		HashSet<Integer> set = new HashSet<>();
		
		set.add(8);
		set.add(9);
		set.add(10);
		set.add(8);
		set.add(50);
		
		System.out.println("Set; " + set);
		
		Set<Integer> set2 = new LinkedHashSet<>();
	}
	
	public void trabajandoConList() {
		List<Integer> lista = new LinkedList<>();
	}
	
	public void trabajandoConLinkedList() {
		LinkedList<Integer> numeros1 = new LinkedList<>();
		
		numeros1.add(3);
		numeros1.addFirst(34);
		numeros1.add(25);
		numeros1.addLast(69);
		numeros1.addFirst(1);
		
		System.out.println("Numeros1: " + numeros1);
		
		System.out.println("Numeros1 Primero: " + numeros1.getFirst());
		System.out.println("Numeros1 Last: " + numeros1.getLast());
		
		System.out.println("Numeros1 Primero: " + numeros1.removeFirst());
		System.out.println("Numeros1: " + numeros1);
		
		System.out.println("Numeros1 Ultimo: " + numeros1.removeLast());
		System.out.println("Numeros1: " + numeros1);
	}
	
	public void trabajandoConArrayList() {
		ArrayList<Integer> numeros1;
		numeros1 = new ArrayList<>();
		
		ArrayList<Integer> numeros2 = new ArrayList<>();
		
		numeros1.add(3);
		numeros1.add(8);
		numeros1.add(25);
		numeros1.add(8);
		
		System.out.println("Numeros1: " + numeros1);
		
		numeros1.add(2, 45);
		
		System.out.println("Numeros1: " + numeros1);
		
		numeros1.remove(0);
		
		System.out.println("Numeros1: " + numeros1);
		
		Integer numero = numeros1.get(1);
		
		System.out.println("El número es: " + numero);
		System.out.println("El número es: " + numeros1.get(3));
		
		System.out.println("Numeros2 esta vacio: " + numeros2.isEmpty()); 
		
		numeros1.set(2, 147);
		
		System.out.println("Numeros1: " + numeros1);
		
		System.out.println("Tamaño Numeros1: " + numeros1.size());
		
		/*for(int i = 0; i < numeros1.size(); i++) {
			System.out.println("Elemento " + i + ": " + numeros1.get(i));
		}*/
		
		for(Integer num : numeros1) {
			System.out.println("Elemento : " + num);
		}
	}

}
